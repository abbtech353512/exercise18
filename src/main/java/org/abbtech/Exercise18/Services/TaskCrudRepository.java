package org.abbtech.Exercise18.Services;

import org.abbtech.Exercise18.Entities.Task;
import org.abbtech.Exercise18.Services.Abstract.ITaskCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TaskCrudRepository implements ITaskCrudRepository {
    private final JdbcTemplate template;

    @Autowired
    public TaskCrudRepository(JdbcTemplate template) {
        this.template = template;
    }

    @Override
    public List<Task> getAllTasks() {
        return null;
    }

    @Override
    public Task getTaskById(int taskId) {
        return null;
    }

    @Override
    public int addTask(Task task) {
        String query = """
                    update tasks set
                    name = ?, description = ? 
                """;
        return template.update(query, task.getName(), task.getDescription());
    }

    @Override
    public void updateTask(Task task) {

    }

    @Override
    public void addTasksBatch(List<Task> tasks) {

    }

    @Override
    public void removeTasksAll() {

    }

    @Override
    public void removeTaskById() {

    }
}
