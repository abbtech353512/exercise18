package org.abbtech.Exercise18.Services.Abstract;

import org.abbtech.Exercise18.Entities.Task;

import java.util.List;

public interface ITaskCrudRepository {
    List<Task> getAllTasks();
    Task getTaskById(int taskId);
    int addTask(Task task);
    void updateTask(Task task);
    void addTasksBatch(List<Task> tasks);
    void removeTasksAll();
    void removeTaskById();
}
