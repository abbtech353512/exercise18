package org.abbtech.Exercise18.Configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.sql.DriverManager;
import java.util.Properties;

@Configuration
public class SpringJDBC_Configuration {
    @Bean
    public DataSource getSource() {
        DriverManagerDataSource dataSource
                = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.abbtech.exercise18.jdbc.driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/abbtech-taskDatabase");
        dataSource.setUsername("postgres");
        dataSource.setPassword("qwer760H");

        return dataSource;
    }
}
