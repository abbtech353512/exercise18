package org.abbtech.Exercise18.Entities;

import org.abbtech.Exercise18.Entities.Abstract.Entity;
@jakarta.persistence.Entity
public class Task extends Entity {
    public Task(String _name, String _description) {
        this.name = _name;
        this.description = _description;
    }
    public Task() {}

    private String name;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

