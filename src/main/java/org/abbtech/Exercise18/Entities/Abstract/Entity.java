package org.abbtech.Exercise18.Entities.Abstract;

public abstract class Entity {
    public int getId() {
        return _id;
    }

    protected void setId(int id) {
        this._id = id;
    }

    private int _id;
    private static int counter = 0;

    protected Entity() {
        _id = counter++;
    }
}
