package junk;

import org.abbtech.Exercise18.Entities.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IJunkRepository extends JpaRepository<Task, Integer> {
}
